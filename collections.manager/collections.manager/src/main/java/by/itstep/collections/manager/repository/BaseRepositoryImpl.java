package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import java.util.List;

public class BaseRepositoryImpl<T> implements BaseRepository<T>{
    private static final String SELECT_TEMPLATE = "SELECT * FROM ";
    private static final String DELETE_TEMPLATE = "DELETE FROM ";

    private Class<T> implClass;
    private String table;

    public BaseRepositoryImpl(Class<T> implClass, String table) {
        this.implClass = implClass;
        this.table = table;
    }

    @Override
    public List<T> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<T> found =
                em.createNativeQuery(getRowsFromTable(), implClass).getResultList();

        em.close();
        return found;
    }

    @Override
    public T findById(Long id) {
        //1 : Получить экземпляр EntityManager`a
        EntityManager em = EntityManagerUtils.getEntityManager();

        //2 : Сделать что нужно
        T foundT = em.find(implClass, id);

        //3 : Закрыть EntityManager`a
        em.close();
        return foundT;
    }

    private String getRowsFromTable() {return SELECT_TEMPLATE + table;}
    private String deleteRowsFromTable() {return DELETE_TEMPLATE + table;}

    @Override
    public T create(T t) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(t);

        em.getTransaction().commit();
        em.close();

        return t;
    }

    @Override
    public T update(T t) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.persist(t);

        em.getTransaction().commit();
        em.close();

        return t;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        T foundT = em.find(implClass, id);
        em.remove(foundT);

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery(deleteRowsFromTable()).executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}
