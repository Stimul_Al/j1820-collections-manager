package by.itstep.collections.manager.repository;


import by.itstep.collections.manager.entity.Collection;

import java.util.List;

public interface CollectionRepository extends BaseRepository<Collection>{
    List<Collection> findByUserId(Long userId);
}
