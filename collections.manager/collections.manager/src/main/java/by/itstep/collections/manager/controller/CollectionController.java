package by.itstep.collections.manager.controller;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.service.AuthService;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.impl.AuthServiceImpl;
import by.itstep.collections.manager.service.impl.CollectionServiceImpl;
import by.itstep.collections.manager.service.UserService;
import by.itstep.collections.manager.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class CollectionController {

    /*
       API:

       1. Получить главную страницу.
       2. Получить страницу по ID коллекции
       3. Получить страницу с формой создания коллекции
       4. Доть возможность создать коллецию из формы

    */

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthService authService;

    @Autowired // -> Спринг заполняет поля найдя в проекте сервис под данной аннотацией
    public CollectionController(CollectionService collectionService, UserService userService, AuthService authService) {
        this.collectionService = collectionService;
        this.userService = userService;
        this.authService = authService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/index") // -> localhost:8080/index
    public String openMainPage(Model model){
        List<CollectionPreviewDto> found = collectionService.findAll();

        if(authService.isAuthenticated()) {
            model.addAttribute("userActiveId", authService.getLoginedUser().getId());
        }
        model.addAttribute("all_collections", found);
        model.addAttribute("logined", authService.getLoginedUser() == null);
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(@PathVariable Long id, Model model){
        if(!authService.isAuthenticated()){
            return "redirect:/sign-in"; // Если кто-то не залогинелся и ломится в профиль
        }
        if(!authService.getLoginedUser().getId().equals(id)) {
            return "redirect:/index"; // Если кто-то ломится в чужой профиль
        }
        List<CollectionPreviewDto> foundCollection = collectionService.findByUserId(id);
        UserFullDto foundUser = userService.findById(id);
        model.addAttribute("all_collections", foundCollection);
        model.addAttribute("user", foundUser);
        model.addAttribute("collectionCreateDto", new CollectionCreateDto());
        return "profile";
    }

    //Нужен чтобы открыть форму
    @RequestMapping(method = RequestMethod.GET, value = "/sign-in") //th: -> localhost:8080/registration
    public String openRegistrationPage(Model model) {

        if(authService.isAuthenticated()) {
            model.addAttribute("userActiveId", authService.getLoginedUser().getId());
        }
        model.addAttribute("createDto", new UserCreateDto());
        model.addAttribute("loginDto", new UserLoginDto());
        return "sign-in";
    }

    // Нужен чтобы принять запрос на сохранине формы
    @RequestMapping(method = RequestMethod.POST, value = "/user/create") // value из Формы
    public String saveUser(UserCreateDto createDto) {
        UserFullDto created = userService.create(createDto);
        return "redirect:/sign-in"; // -> localhost:8080/profile/13 -> openProfile()
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user/login")
    public String login(UserLoginDto loginDto) {
        authService.login(loginDto); // Если логин + пароль подошли, то он будет залогинен
        return "redirect:/profile/" + authService.getLoginedUser().getId(); //TODO
    }

    @RequestMapping(method = RequestMethod.POST, value = "/collection/create")
    public String saveCollection(CollectionCreateDto createDto) {
        try {
            collectionService.create(createDto);
        } catch (InvalidDtoException e) {
            e.printStackTrace();
            return "redirect:/oops"; // <- Страница с ошибкой!
        }
        return "redirect:/profile/" + authService.getLoginedUser().getId();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/oops")
    public String showErorPage() {
        return "funny-eror";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/logout")
    public String logout(){
        authService.logout();
        return "redirect:/index";
    }
}
