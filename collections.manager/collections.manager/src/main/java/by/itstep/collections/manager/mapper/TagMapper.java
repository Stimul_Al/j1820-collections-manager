package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TagMapper {

    public List<TagPreviewDto> mapToDtoTags(List<Tag> entities) {
        List<TagPreviewDto> dtoList = new ArrayList<>();

        for (Tag tag :
                entities) {
            TagPreviewDto dto = new TagPreviewDto();

            dto.setName(tag.getName());

            dtoList.add(dto);
        }

        return dtoList;
    }

    public Tag mapToEntity(TagUpdateDto updateDto) {
        Tag tag = new Tag();

        tag.setId(updateDto.getId());
        tag.setName(updateDto.getName());

        return tag;
    }

    public Tag mapToEntity(TagCreateDto createDto) {
        Tag tag = new Tag();

        tag.setName(createDto.getName());

        return tag;
    }



    public TagFullDto mapToDto(Tag tag) {
        TagFullDto fullDto = new TagFullDto();

        fullDto.setId(tag.getId());
        fullDto.setName(tag.getName());

        return fullDto;
    }
}
