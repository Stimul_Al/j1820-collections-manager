package by.itstep.collections.manager.dto.comment;

import lombok.Data;

@Data
public class CommentCreateDto {

    private String message;

}
