package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.entity.Role;
import lombok.Data;

@Data
public class UserPreviewDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private Role role;

}
