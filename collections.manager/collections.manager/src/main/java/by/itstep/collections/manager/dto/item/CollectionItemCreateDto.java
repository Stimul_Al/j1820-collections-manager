package by.itstep.collections.manager.dto.item;

import lombok.Data;

@Data
public class CollectionItemCreateDto {

    private String name;

}
