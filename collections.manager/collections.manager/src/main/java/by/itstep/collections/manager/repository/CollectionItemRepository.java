package by.itstep.collections.manager.repository;


import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;

public interface CollectionItemRepository extends BaseRepository<CollectionItem> {
}
