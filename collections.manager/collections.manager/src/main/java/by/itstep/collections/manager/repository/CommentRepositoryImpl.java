package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import org.springframework.stereotype.Repository;

@Repository
public class CommentRepositoryImpl extends BaseRepositoryImpl<Comment> implements CommentRepository {

    public CommentRepositoryImpl() {
        super(Comment.class, "`comments`");
    }
}
