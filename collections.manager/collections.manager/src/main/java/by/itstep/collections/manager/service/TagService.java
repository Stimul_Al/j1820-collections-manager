package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;

import java.util.List;

public interface TagService {
    //1. Найти все
    List<TagPreviewDto> findAll();

    //2. Найти по ID
    TagFullDto findById(Long id);

    //3. Сохранить новый
    TagFullDto create(TagCreateDto createDto);

    //4. Обновить существующий
    TagFullDto update(TagUpdateDto updateDto);

    //5. Удалить по ID
    void deleteById(Long id);
}
