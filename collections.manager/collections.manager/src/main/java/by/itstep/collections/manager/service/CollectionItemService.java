package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.dto.item.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.item.CollectionItemFullDto;
import by.itstep.collections.manager.dto.item.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.item.CollectionItemUpdateDto;

import java.util.List;

public interface CollectionItemService {

    //1. Найти все
    List<CollectionItemPreviewDto> findAll();

    //2. Найти по ID
    CollectionItemFullDto findById(Long id);

    //3. Сохранить новый
    CollectionItemFullDto create(CollectionItemCreateDto createDto);

    //4. Обновить существующий
    CollectionItemFullDto update(CollectionItemUpdateDto updateDto);

    //5. Удалить по ID
    void deleteById(Long id);

}
