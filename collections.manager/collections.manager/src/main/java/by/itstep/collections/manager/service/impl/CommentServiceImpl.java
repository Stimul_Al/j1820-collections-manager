package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.mapper.CommentMapper;
import by.itstep.collections.manager.repository.BaseRepository;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.repository.CommentRepositoryImpl;
import by.itstep.collections.manager.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private BaseRepository<Comment> commentRepository;

    @Autowired
    private CommentMapper mapper = new CommentMapper();


    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> comments = commentRepository.findAll();

        return mapper.mapToDtoComments(comments);
    }

    @Override
    public CommentFullDto findById(Long id) {
        Comment comment = commentRepository.findById(id);

        return mapper.mapToFullDto(comment);
    }

    @Override
    public CommentFullDto create(CommentCreateDto createDto) {
        Comment comment = mapper.mapToEntity(createDto);

        Comment commentToCreate = commentRepository.create(comment);

        return mapper.mapToFullDto(commentToCreate);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {
        Comment comment = mapper.mapToEntity(updateDto);

        Comment commentToUpdate = commentRepository.create(comment);

        return mapper.mapToFullDto(commentToUpdate);
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
    }
}
