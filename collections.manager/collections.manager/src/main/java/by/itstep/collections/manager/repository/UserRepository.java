package by.itstep.collections.manager.repository;


import by.itstep.collections.manager.entity.User;

public interface UserRepository {
    User findByEmail(String email);
}
