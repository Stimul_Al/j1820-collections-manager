package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.entity.Role;
import by.itstep.collections.manager.entity.User;

public interface AuthService {

    void login(UserLoginDto loginDto);

    void logout();

    boolean isAuthenticated();

    public Role getRole();

    User getLoginedUser();

}
