package by.itstep.collections.manager.dto.comment;

import lombok.Data;

@Data
public class CommentUpdateDto {

    private Long id;
    private String message;

}
