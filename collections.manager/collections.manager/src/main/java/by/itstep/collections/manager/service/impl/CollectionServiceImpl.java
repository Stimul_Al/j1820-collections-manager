package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.BaseRepository;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import by.itstep.collections.manager.repository.UserRepositoryImpl;
import by.itstep.collections.manager.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    private BaseRepository<User> userRepository;

    @Autowired
    private CollectionMapper mapper;


    @Override
    public List<CollectionPreviewDto> findByUserId(Long userId) {
        List<Collection> found = collectionRepository.findByUserId(userId);
        System.out.println("Collections from user -> found : " + found.size() + "collections");
        return mapper.mapToDtoCollections(found);
    }

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + " collections");
        return mapper.mapToDtoCollections(found);
    }

    @Override
    public CollectionFullDto findById(Long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found collection" + found);
        return mapper.mapToFullDto(found);
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) throws InvalidDtoException {
        validateCreateDto(createDto);
        User user = getUserId(createDto.getUserId());
        Collection toSave = mapper.mapToEntity(createDto, user);

        Collection entityToCreated = collectionRepository.create(toSave);
        System.out.println("CollectionServiceImpl -> created collection" + entityToCreated);
        return mapper.mapToFullDto(entityToCreated);
    }

    private User getUserId(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        validateUpdateDto(updateDto);
        Collection entityToUpdated = mapper.mapToEntity(updateDto);
        Collection existingEntity = collectionRepository.findById(updateDto.getId());

        entityToUpdated.setImageUrl(existingEntity.getImageUrl());
        entityToUpdated.setUser(existingEntity.getUser());

        System.out.println("CollectionServiceImpl -> updated collection" + entityToUpdated);

        return mapper.mapToFullDto(entityToUpdated);
    }

    @Override
    public void deleteById(Long id) {
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl -> collection with id " + id + " was deleted");
    }

    private void validateCreateDto(CollectionCreateDto createDto) throws InvalidDtoException {
        if (createDto.getUserId() == null || createDto.getDescription() == null ||
                createDto.getName() == null || createDto.getTitle() == null) {
            throw new InvalidDtoException("One or more fields is null");
        }

        if (createDto.getDescription().length() < 15) {
            throw new InvalidDtoException("Description min length is 15");
        }

    }

    private void validateUpdateDto(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        if(updateDto.getId() == null) {
            throw new MissedUpdateIdException("Collection id is not specified");
        }
    }
}
