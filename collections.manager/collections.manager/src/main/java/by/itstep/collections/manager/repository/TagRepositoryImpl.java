package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TagRepositoryImpl extends BaseRepositoryImpl<Tag> implements TagRepository {

    public TagRepositoryImpl() {
        super(Tag.class, "`tags`");
    }
}
