package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.item.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.item.CollectionItemFullDto;
import by.itstep.collections.manager.dto.item.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.item.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.BaseRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.service.CollectionItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollectionItemServiceImpl implements CollectionItemService {

    @Autowired
    private BaseRepository<CollectionItem> itemRepository;

    @Autowired
    private CollectionItemMapper mapper;

    @Override
    public List<CollectionItemPreviewDto> findAll() {
        List<CollectionItem> items = itemRepository.findAll();

        return mapper.mapToDtoItems(items);
    }

    @Override
    public CollectionItemFullDto findById(Long id) {
        CollectionItem foundItem = itemRepository.findById(id);

        return mapper.mapToFullDto(foundItem);
    }

    @Override
    public CollectionItemFullDto create(CollectionItemCreateDto createDto) {
        CollectionItem item = mapper.mapToEntity(createDto);

        CollectionItem itemCreated = itemRepository.create(item);
        return mapper.mapToFullDto(itemCreated);
    }

    @Override
    public CollectionItemFullDto update(CollectionItemUpdateDto updateDto) {
        CollectionItem item = mapper.mapToEntity(updateDto);

        CollectionItem itemUpdate = itemRepository.update(item);
        return mapper.mapToFullDto(itemUpdate);
    }

    @Override
    public void deleteById(Long id) {
        itemRepository.deleteById(id);
    }
}
