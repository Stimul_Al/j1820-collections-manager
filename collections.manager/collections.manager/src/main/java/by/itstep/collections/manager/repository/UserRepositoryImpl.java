package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class UserRepositoryImpl extends BaseRepositoryImpl<User> implements UserRepository {

    public UserRepositoryImpl() {
        super(User.class, "`users`");
    }

    @Override
    public User findByEmail(String email) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        User found = (User) em.createNativeQuery(
                String.format("SELECT * FROM `users` WHERE `email` = \"%s\"", email), User.class).getSingleResult();
        em.close();
        System.out.println("Found: " + found);
        return found;
    }
}
