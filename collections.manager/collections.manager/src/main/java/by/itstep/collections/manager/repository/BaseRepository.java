package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;

import java.util.List;

public interface BaseRepository<T> {

    //1. Найти все
    List<T> findAll();

    //2. Найти по ID
    T findById(Long id);

    //3. Сохранить новый
    T create(T t);

    //4. Обновить существующий
    T update(T t);

    //5. Удалить по ID
    void deleteById(Long id);

    //6. Удалить все эелементы
    void deleteAll();
}
