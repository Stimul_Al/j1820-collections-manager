package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;

import java.util.List;

public interface CommentService {

    //1. Найти все
    List<CommentPreviewDto> findAll();

    //2. Найти по ID
    CommentFullDto findById(Long id);

    //3. Сохранить новый
    CommentFullDto create(CommentCreateDto createDto);

    //4. Обновить существующий
    CommentFullDto update(CommentUpdateDto updateDto);

    //5. Удалить по ID
    void deleteById(Long id);
    
}
