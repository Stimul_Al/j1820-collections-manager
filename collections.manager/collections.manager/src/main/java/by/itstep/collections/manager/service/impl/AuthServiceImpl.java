package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.user.UserLoginDto;
import by.itstep.collections.manager.entity.Role;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;
import by.itstep.collections.manager.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class AuthServiceImpl implements AuthService {

    private User loginedUser; // <- трон

    @Autowired
    private UserRepository userRepository;


    @Override
    public void login(UserLoginDto loginDto) {
        //1. Найти юзера по емайл
            //1.1 Если не нашли - ошибку такого нет
        User foundUser = userRepository.findByEmail(loginDto.getEmail());
        if (foundUser == null) {
            throw new RuntimeException("User not found by email: " + foundUser.getId());
        }
        //2. Сравнить пароли из dto и найденного
            //2.1 Если не совпали - ошибка пароли не совпали
        if(!foundUser.getPassword().equals(loginDto.getPassword())) {
            throw new RuntimeException("User password is incorrect!");
        }
        //3. пометить юзера как залогиненным
        loginedUser = foundUser; // <- Сохранаили как залогиненого!
    }

    @Override
    public void logout() {
        loginedUser = null;
    }

    @Override
    public boolean isAuthenticated() {
        return loginedUser != null;
    }

    @Override
    public Role getRole() {
        return loginedUser.getRole();
    }

    @Override
    public User getLoginedUser() {
        return loginedUser;
    }
}
