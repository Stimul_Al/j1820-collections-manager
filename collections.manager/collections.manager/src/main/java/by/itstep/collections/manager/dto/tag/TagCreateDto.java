package by.itstep.collections.manager.dto.tag;

import lombok.Data;

@Data
public class TagCreateDto {

    private String name;

}
