package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.comment.CommentCreateDto;
import by.itstep.collections.manager.dto.comment.CommentFullDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentUpdateDto;
import by.itstep.collections.manager.entity.Comment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentMapper {


    public List<CommentPreviewDto> mapToDtoComments(List<Comment> comments) {
        List<CommentPreviewDto> dtoList = new ArrayList<>();

        for (Comment comment : comments) {
            CommentPreviewDto dto = new CommentPreviewDto();

            dto.setUserName(comment.getUser().getName());
            dto.setMessage(comment.getMessage());
            dto.setCreatedAt(comment.getCreatedAt());

            dtoList.add(dto);
        }
        return dtoList;
    }

    public CommentFullDto mapToFullDto(Comment comment) {
        CommentFullDto fullDto = new CommentFullDto();

        fullDto.setId(comment.getId());
        fullDto.setMessage(comment.getMessage());
        fullDto.setCreateAt(comment.getCreatedAt());
        fullDto.setCollection(comment.getCollection());
        fullDto.setUserName(comment.getUser().getName() + " " + comment.getUser().getLastName());

        return fullDto;
    }

    public Comment mapToEntity(CommentUpdateDto updateDto) {
        Comment comment = new Comment();

        comment.setId(updateDto.getId());
        comment.setMessage(updateDto.getMessage());

        return comment;
    }

    public Comment mapToEntity(CommentCreateDto createDto) {
        Comment comment = new Comment();

        comment.setMessage(createDto.getMessage());

        return comment;
    }

}
