package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserMapper {

    public List<UserPreviewDto> mapToDtoUsers(List<User> entities) {
        List<UserPreviewDto> dtoList = new ArrayList<>();

        for (User user :
                entities) {
            UserPreviewDto dto = new UserPreviewDto();

            dto.setId(user.getId());
            dto.setName(user.getName());
            dto.setLastName(user.getLastName());
            dto.setEmail(user.getEmail());
            dto.setRole(user.getRole());

            dtoList.add(dto);
        }

        return dtoList;
    }

    public UserFullDto mapToDtoFullUser(User user) {
        UserFullDto dtoFullUser = new UserFullDto();

        dtoFullUser.setId(user.getId());
        dtoFullUser.setName(user.getName());
        dtoFullUser.setLastName(user.getLastName());
        dtoFullUser.setEmail(user.getEmail());
        dtoFullUser.setRole(user.getRole());

        return dtoFullUser;
    }

    public User mapToEntity(UserCreateDto createDto) {
        User user = User.builder()
                .name(createDto.getName())
                .lastName(createDto.getLastName())
                .email(createDto.getEmail())
                .password(createDto.getPassword())
                .build();

        return user;
    }

    public User mapToEntity(UserUpdateDto updateDto) {
        User user = User.builder()
                .id(updateDto.getId())
                .name(updateDto.getName())
                .lastName(updateDto.getLastName())
                .email(updateDto.getEmail())
                .build();

        return user;
    }

}
