package by.itstep.collections.manager.service;


import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.User;

import java.util.List;

public interface UserService {
    //1. Найти все
    List<UserPreviewDto> findAll();

    //2. Найти по ID
    UserFullDto findById(Long id);

    //3. Сохранить новый
    UserFullDto create(UserCreateDto createDto);

    //4. Обновить существующий
    UserFullDto update(UserUpdateDto updateDto);

    //5. Удалить по ID
    void deleteById(Long id);

    UserFullDto findByEmail(String email);
}
