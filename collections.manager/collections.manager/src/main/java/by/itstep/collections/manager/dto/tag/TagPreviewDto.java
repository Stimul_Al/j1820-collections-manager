package by.itstep.collections.manager.dto.tag;

import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import lombok.Data;

import java.util.List;

@Data
public class TagPreviewDto {

    private String name;
    private List<CollectionPreviewDto> collection;

}
