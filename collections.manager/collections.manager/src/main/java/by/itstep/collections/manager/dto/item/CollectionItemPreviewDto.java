package by.itstep.collections.manager.dto.item;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.List;

@Data
public class CollectionItemPreviewDto {

    private Long id;
    private String name;
    private Collection collection;

}
