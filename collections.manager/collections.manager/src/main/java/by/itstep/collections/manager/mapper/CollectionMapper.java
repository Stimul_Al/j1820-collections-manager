package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollectionMapper {

    public List<CollectionPreviewDto> mapToDtoCollections(List<Collection> entities) {
     List<CollectionPreviewDto> dtoList = new ArrayList<>();
        /**
         * 0. В цикле :
         *      1. Соз  дать новый DTO
         *      2. Перелить из него данные из ENTITY
         *      3. Добавить полученный DTO в список
         */
        for (Collection entity: entities) {
            CollectionPreviewDto dto = new CollectionPreviewDto();

            dto.setId(entity.getId());
            dto.setImageUrl(entity.getImageUrl());
            dto.setName(entity.getName());
            dto.setTags(entity.getTags());
            dto.setTitle(entity.getTitle());
            dto.setDescription(entity.getDescription());
            dto.setUserName(entity.getUser().getName() + " " +
                            entity.getUser().getLastName());

            dtoList.add(dto);
        }
     return dtoList;
    }

    public CollectionFullDto mapToFullDto(Collection collection) {
        CollectionFullDto fullDto = new CollectionFullDto();

        fullDto.setId(collection.getId());
        fullDto.setName(collection.getName());
        fullDto.setTitle(collection.getTitle());
        fullDto.setDescription(collection.getDescription());
        fullDto.setImageUrl(collection.getImageUrl());
        fullDto.setUserId(collection.getUser().getId());

        return fullDto;
    }

    public Collection mapToEntity(CollectionCreateDto createDto, User user) {
        Collection collection = Collection.builder()
                .description(createDto.getDescription())
                .name(createDto.getName())
                .title(createDto.getTitle())
                .imageUrl(createDto.getImageUrl())
                .user(user)
                .build();

        return collection;
    }

    public Collection mapToEntity(CollectionUpdateDto updateDto) {
        Collection collection = Collection.builder()
                .id(updateDto.getId())
                .description(updateDto.getDescription())
                .name(updateDto.getName())
                .title(updateDto.getTitle())
                .build();

        return collection;
    }

}
