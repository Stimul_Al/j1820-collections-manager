package by.itstep.collections.manager.dto.tag;

import lombok.Data;

@Data
public class TagUpdateDto {

    private Long id;
    private String name;

}
