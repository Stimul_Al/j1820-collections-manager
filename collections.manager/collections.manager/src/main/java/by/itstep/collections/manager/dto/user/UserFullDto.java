package by.itstep.collections.manager.dto.user;

import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.comment.CommentPreviewDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Role;
import lombok.Data;


import java.util.List;

@Data
public class UserFullDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private List<CollectionPreviewDto> collections;
    private List<CommentPreviewDto> comments;
    private Role role;

}
