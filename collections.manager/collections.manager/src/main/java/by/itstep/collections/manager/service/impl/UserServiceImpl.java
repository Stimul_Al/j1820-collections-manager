package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.user.UserCreateDto;
import by.itstep.collections.manager.dto.user.UserFullDto;
import by.itstep.collections.manager.dto.user.UserPreviewDto;
import by.itstep.collections.manager.dto.user.UserUpdateDto;
import by.itstep.collections.manager.entity.Role;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.BaseRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;
import by.itstep.collections.manager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private BaseRepository<User> userRepository;

    @Autowired
    private UserMapper mapper = new UserMapper();

    @Override
    public List<UserPreviewDto> findAll() {
        List<User> found = userRepository.findAll();
        System.out.println("UserServiceImpl -> " + found.size() + " user");
        return mapper.mapToDtoUsers(found);
    }

    @Override
    public UserFullDto findByEmail(String email) {
        return new UserFullDto();
    }

    @Override
    public UserFullDto findById(Long id) {
        User found = userRepository.findById(id);

        System.out.println("UserServiceRepositoryImppl -> found user " + found);
        return mapper.mapToDtoFullUser(found);
    }

    @Override
    public UserFullDto create(UserCreateDto createDto) {
        User toSave = mapper.mapToEntity(createDto);

        User userToCreated = userRepository.create(toSave);
        userToCreated.setRole(Role.USER);

        System.out.println("UserServiceImpl -> created user" + userToCreated);
        return mapper.mapToDtoFullUser(userToCreated);
    }

    @Override
    public UserFullDto update(UserUpdateDto updateDto) {
        User userExisting = userRepository.findById(updateDto.getId());
        User user = mapper.mapToEntity(updateDto);

        user.setCollections(userExisting.getCollections());
        user.setComments(userExisting.getComments());
        user.setRole(userExisting.getRole());

        User updated = userRepository.update(user);

        System.out.println("UserServiceImpl -> updated user" + updated);
        return mapper.mapToDtoFullUser(updated);
    }

    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);
        System.out.println("UserServiceImpl -> user with id" + id + " was deleted" );
    }
}
