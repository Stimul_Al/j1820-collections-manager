package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import org.springframework.stereotype.Repository;

@Repository
public class CollectionItemRepositoryImpl
        extends BaseRepositoryImpl<CollectionItem> implements CollectionItemRepository {

    public CollectionItemRepositoryImpl() {
        super(CollectionItem.class, "`collection_items`");
    }
}
