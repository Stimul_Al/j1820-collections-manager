package by.itstep.collections.manager.service.impl;

import by.itstep.collections.manager.dto.tag.TagCreateDto;
import by.itstep.collections.manager.dto.tag.TagFullDto;
import by.itstep.collections.manager.dto.tag.TagPreviewDto;
import by.itstep.collections.manager.dto.tag.TagUpdateDto;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.mapper.TagMapper;
import by.itstep.collections.manager.repository.BaseRepository;
import by.itstep.collections.manager.repository.TagRepositoryImpl;
import by.itstep.collections.manager.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private BaseRepository<Tag> tagRepository;

    @Autowired
    private TagMapper mapper = new TagMapper();

    @Override
    public List<TagPreviewDto> findAll() {
        List<Tag> tags = tagRepository.findAll();
        return mapper.mapToDtoTags(tags);
    }

    @Override
    public TagFullDto findById(Long id) {
        Tag tag = tagRepository.findById(id);
        return mapper.mapToDto(tag);
    }

    @Override
    public TagFullDto create(TagCreateDto createDto) {
        Tag tag = mapper.mapToEntity(createDto);

        Tag tagToCreate = tagRepository.create(tag);

        return mapper.mapToDto(tagToCreate);
    }

    @Override
    public TagFullDto update(TagUpdateDto updateDto) {

        Tag tag = mapper.mapToEntity(updateDto);

        Tag tagToUpdate = tagRepository.update(tag);

        return mapper.mapToDto(tagToUpdate);
    }

    @Override
    public void deleteById(Long id) {
        tagRepository.deleteById(id);
    }
}
