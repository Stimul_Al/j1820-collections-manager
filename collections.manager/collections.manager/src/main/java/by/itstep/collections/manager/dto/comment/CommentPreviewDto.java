package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Tag;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class CommentPreviewDto {

    private String message;
    private String userName;
    private Date createdAt;

}
