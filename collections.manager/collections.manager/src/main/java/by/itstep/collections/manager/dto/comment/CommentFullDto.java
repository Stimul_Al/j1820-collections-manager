package by.itstep.collections.manager.dto.comment;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;

import java.util.Date;

@Data
public class CommentFullDto {

    private Long id;
    private String message;
    private String userName;
    private Date createAt;
    private Collection collection;

}
