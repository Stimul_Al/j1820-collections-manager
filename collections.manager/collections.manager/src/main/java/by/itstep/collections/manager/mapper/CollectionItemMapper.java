package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.item.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.item.CollectionItemFullDto;
import by.itstep.collections.manager.dto.item.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.item.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.BaseRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CollectionItemMapper {

    public List<CollectionItemPreviewDto> mapToDtoItems(List<CollectionItem> items) {
        List<CollectionItemPreviewDto> itemsDto = new ArrayList<>();
        for(CollectionItem item : items) {
            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();

            dto.setId(item.getId());
            dto.setName(item.getName());
            dto.setCollection(item.getCollection());

            itemsDto.add(dto);
        }
        return itemsDto;
    }

    public CollectionItemFullDto mapToFullDto(CollectionItem item){
        CollectionItemFullDto fullDto = new CollectionItemFullDto();

        fullDto.setId(item.getId());
        fullDto.setName(item.getName());
        fullDto.setCollection(item.getCollection());

        return fullDto;
    }

    public CollectionItem mapToEntity(CollectionItemUpdateDto updateDto) {
        CollectionItem item = new CollectionItem();

        item.setId(updateDto.getId());
        item.setName(updateDto.getName());
        item.setCollection(updateDto.getCollection());

        return item;
    }

    public CollectionItem mapToEntity(CollectionItemCreateDto createDto) {
        CollectionItem item = new CollectionItem();

        item.setName(createDto.getName());

        return item;
    }



}
