package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CollectionRepositoryImpl extends BaseRepositoryImpl<Collection> implements CollectionRepository {

    public CollectionRepositoryImpl() {
        super(Collection.class, "`collections`");
    }

    @Override
    public List<Collection> findByUserId(Long userId) {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Collection> foundCollections = em.createNativeQuery(
                String.format("SELECT * FROM `collections` WHERE `user_id` = \"%s\"", userId), Collection.class).getResultList();

        em.close();
        return foundCollections;
    }
}
