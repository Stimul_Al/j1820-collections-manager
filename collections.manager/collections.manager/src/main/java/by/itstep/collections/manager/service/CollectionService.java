package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.collection.CollectionCreateDto;
import by.itstep.collections.manager.dto.collection.CollectionFullDto;
import by.itstep.collections.manager.dto.collection.CollectionPreviewDto;
import by.itstep.collections.manager.dto.collection.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;

import java.util.List;

public interface CollectionService {

    //1. Найти все
    List<CollectionPreviewDto> findAll();

    //2. Найти по ID
    CollectionFullDto findById(Long id);

    //3. Сохранить новый
    CollectionFullDto create(CollectionCreateDto createDto) throws InvalidDtoException;

    //4. Обновить существующий
    CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException;

    //5. Удалить по ID
    void deleteById(Long id);

    //Поиск коллекций по ID user-a
    List<CollectionPreviewDto> findByUserId(Long userId);
}
