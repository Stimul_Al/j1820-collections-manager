package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ApplicationTests {

	private CollectionRepositoryImpl collectionRepository = new CollectionRepositoryImpl();
	private CollectionItemRepositoryImpl collectionItemRepository = new CollectionItemRepositoryImpl();

	@BeforeEach
	void setup() {
		collectionItemRepository.deleteAll();
		collectionRepository.deleteAll();
	}

	@Test
	void testCreate() {
		// given
		Collection collection = Collection.builder()
				.description("my-description")
				.imageUrl("my-image")
				.title("my-title")
				.name("my-name")
				.build();

		// when
		Collection saved = collectionRepository.create(collection);

		// then

		Assertions.assertNotNull(saved.getId()); // убеждаемся, что ID не null
	}

	@Test
	void testFindAll() {
		//given
		Collection collectionOne = Collection.builder()
				.description("my-description1")
				.imageUrl("my-image1")
				.title("my-title1")
				.name("my-name1")
				.build();

		Collection collectionTwo = Collection.builder()
				.description("my-description2")
				.imageUrl("my-image2")
				.title("my-title2")
				.name("my-name2")
				.build();
		collectionRepository.create(collectionOne);
		collectionRepository.create(collectionTwo);

		//when
		List<Collection> foundCollections = collectionRepository.findAll();

		//then
		Assertions.assertEquals(foundCollections.size(), 2);
	}

	@Test
	void save_collectionWithItems() {
		//given
		Collection c = Collection.builder()
				.name("asd")
				.title("ASD")
				.imageUrl("qweqe")
				.description("qqwwee")
				.build();
		Collection saved = collectionRepository.create(c);


		CollectionItem i1 = CollectionItem.builder()
				.name("item 1")
				.collection(saved)
				.build();

		CollectionItem i2 = CollectionItem.builder()
				.name("item 2")
				.collection(saved)
				.build();

		//when

		CollectionItem savedI1 = collectionItemRepository.create(i1);
		CollectionItem savedI2 = collectionItemRepository.create(i2);
		List<CollectionItem> items = List.of(i1, i2);

		//then
		Assertions.assertNotNull(saved.getId());

	}

	@Test
	void findById_happyPast() {
		// given
		Collection c = Collection.builder()
				.name("asd")
				.title("ASD")
				.imageUrl("qweqe")
				.description("qqwwee")
				.build();

		Collection saved = collectionRepository.create(c);


		CollectionItem i1 = CollectionItem.builder()
				.name("item 1")
				.collection(saved)
				.build();

		CollectionItem i2 = CollectionItem.builder()
				.name("item 2")
				.collection(saved)
				.build();

		CollectionItem savedI1 = collectionItemRepository.create(i1);
		CollectionItem savedI2 = collectionItemRepository.create(i2);

		// when
		Collection found = collectionRepository.findById(saved.getId());

		// then
		Assertions.assertNotNull(found);
		Assertions.assertNotNull(found.getId());
		Assertions.assertNotNull(found.getItems());
		Assertions.assertEquals(2,found.getItems().size());
	}

}

/*

	@OneToMany
	1. Если у One указан список дочерней при сохранении, то это ни на что не влияет!
	2. Если при сохранении одного их MANY у него проставлен ONE без ID, то будет ошибка.

	3. Чтобы проставить FK, нужно сначала сохранить ONE, а потом использовать
	его для сохранения каждого из MANY по отдельности.

*/